#include <stdio.h>

typedef struct Person{
    char * name;
    int age;
}Person;

int main()
{
    Person John;
    John.name = "John";
    John.age = 23;
    
    printf("They are %s with %d years old\n", John.name, John.age);
    
    return 0;
}
