// By default, variables are local to the scope in which they are defined.
// Variables can be declared as static to increase their scope up to file
// containing them. As a result, these variables can be accessed anywhere inside
// a file.

#include<stdio.h>
int runner() {
    static int count = 0;
    count++;
    return count;
}

int sum(int num){
    static int sum;
    sum += num;
    return sum;
}

int main()
{
    printf("%d ", runner());
    printf("%d ", runner());
    
    printf("%d ", sum(55));
    printf("%d ", sum(45));
    printf("%d ", sum(50));
}

