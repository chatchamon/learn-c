//
//  main.c
//  union
//
//  Created by chatchamon on 3/9/2563 BE.
//  Copyright © 2563 nectec. All rights reserved.
//

#include <stdio.h>

union intParts{
    int theInt;
    char byte[sizeof(int)];
};

int main() {
    union intParts parts;
    parts.theInt = 5968145;
    
    printf("The int is %i\nThe bytes are [%i, %i, %i, %i]\n", parts.theInt, parts.byte[0], parts.byte[1], parts.byte[2], parts.byte[3]);
    
    int theInt = parts.theInt;
    printf("The int is %i\nThe bytes are [%i,%i,%i,%i]\n", theInt, *((char*)&theInt+0), *((char*)&theInt+1), *((char*)&theInt+2), *((char*)&theInt+3));
    
    
    printf("The int is %i\nThe bytes are [%i,%i,%i,%i]\n", theInt, ((char*) &theInt)[0], ((char*) &theInt)[1], ((char *) &theInt)[2], ((char*) &theInt)[3]);
    
    
    return 0;
}
