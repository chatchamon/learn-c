//
//  malloc.h
//  malloc
//
//  Created by chatchamon on 3/9/2563 BE.
//  Copyright © 2563 nectec. All rights reserved.
//

#ifndef malloc_h
#define malloc_h

#include <stdio.h>

#endif /* malloc_h */

typedef struct Point2D{
    int x;
    int y;
}Point2D;

void print_point_ptr(Point2D* ptr){
    printf("x: %d\n", ptr->x);
    printf("y: %d\n", ptr->y);
}

void print_point_inst(Point2D inst){
    printf("x: %d\n", inst.x);
    printf("y: %d\n", inst.y);
}

