// this exercise tell you that
// -> access object member varaible and method via POINTER
// .  access object member varaible and method via INSTANCE


#include <stdio.h>
#include <stdlib.h>
#include "malloc.h"

int main(){
    Point2D *mypoint_ptr;
    Point2D mypoint_inst;
    
    mypoint_ptr = (Point2D *)malloc(sizeof(Point2D));
    mypoint_ptr -> x = 5;
    mypoint_ptr -> y = 10;
    print_point_ptr(mypoint_ptr);
    
    mypoint_inst.x = 11;
    mypoint_inst.y = 12;
    print_point_inst(mypoint_inst);
    
    //return 0;
}
