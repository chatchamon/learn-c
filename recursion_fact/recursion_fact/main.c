//
//  main.c
//  recursion_fact
//
//  Created by chatchamon on 3/9/2563 BE.
//  Copyright © 2563 nectec. All rights reserved.
//

#include <stdio.h>

int factorial(int num){
    if(num == 1)
        return num;
    
    else if(num > 0){
        return num * factorial(num-1);
    }
    return 0;
}


int main() {
    printf("1! = %i\n", factorial(1));
    printf("3! = %i\n", factorial(3));
    printf("5! = %i\n", factorial(5));
    return 0;
}
