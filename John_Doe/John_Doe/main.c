#include <stdio.h>
#include <string.h>
int main() {
  char * first_name = "John";
  char last_name[] = "Doe";
  char name[100];

  //changed first letter of last name
  last_name[0] = 'B';
  sprintf(name, "%s %s", first_name, last_name);
    
  //compare if the name is "John Boe"
  if(strncmp(name, "John Boe", 100) == 0)
      printf("Done!\n");

  //using strncat
    printf("%s\n", name);
    name[0] = '\0';
    printf("%s", name);
    
    strncat(name, first_name, 4);
    printf("name: %s, first_name: %s, last_name: %s\n", name, first_name, last_name);
    strncat(name, last_name, 20);
    printf("name: %s, first_name: %s, last_name: %s\n", name, first_name, last_name);

    return 0;
}
