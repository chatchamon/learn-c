#include <stdio.h>

int main(){
    int a[] = {1,2,3,4,5,6,7,8,9,0};
    
    printf("%zu\n", sizeof(a));
    printf("%zu\n", sizeof(*a));
    printf("%zu\n", sizeof(a)/sizeof(*a));
    
    size_t sz = sizeof(a)/sizeof(*a);
    
    printf("then length of this array is: %zu\n", sz);
    
    //size_t sz = sizeof a / sizeof *a;
    
    //printf("%zu\n", sz);
}


