#include <stdio.h>
#include "binary_tree.h"

void insert(node_t *tree, int val);
void printDFS(node_t *tree);

int main(){
    node_t *test_tree = (node_t *) malloc(sizeof(node_t));
    test_tree->val = 0;
    test_tree->left = NULL;
    test_tree -> right = NULL;
    
    insert(test_tree, 5);
    insert(test_tree, 8);
    insert(test_tree, 4);
    insert(test_tree, 3);
    
    printDFS(test_tree);
    
}



