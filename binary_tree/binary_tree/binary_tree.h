//
//  binary_tree.h
//  binary_tree
//
//  Created by chatchamon on 3/9/2563 BE.
//  Copyright © 2563 nectec. All rights reserved.
//

#ifndef binary_tree_h
#define binary_tree_h

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int val;
    struct node *left;
    struct node *right;
}node_t;

void insert(node_t *tree, int val){
    //in the initial state, where val = 0
    if(tree->val == 0)
        tree->val = val;
    else
    {
        //if new value is less than current value, we need to check left side branch
        if(val < tree->val)
        {
            if(tree->left != NULL)
                insert(tree->left, val);
            else{
                tree->left = (node_t *) malloc(sizeof(node_t));
                tree->left->val = val;
                tree->left->left = NULL;
                tree->left->right = NULL;
            }
        }
        //if new value is more than current value, we need to check right side branch
        else if(val > tree->val)
        {
            if(tree->right != NULL)
                insert(tree->right, val);
            else
            {
                tree->right = (node_t *) malloc(sizeof(node_t));
                tree->right->val = val;
                tree->right->left = NULL;
                tree->right->right = NULL;
            }
        }
    }
}

void printDFS(node_t *current){
    if(current == NULL)         return;
    if(current != NULL)         printf("%d\t", current->val);
    if(current->left != NULL)   printDFS(current->left);
    if(current->right != NULL)  printDFS(current->right);
}

#endif /* binary_tree_h */
