//
//  main.c
//  factorial
//
//  Created by chatchamon on 2/9/2563 BE.
//  Copyright © 2563 nectec. All rights reserved.
//

#include <stdio.h>

int main()
{
    int array[] = {1,2,3,4,5,6,7,8,9,10};
    int factorial = 1;
    
    for(int i = 1; i < 10; i++)
        factorial *= i;
    
    printf("10! is %d.\n", factorial);
    
}
