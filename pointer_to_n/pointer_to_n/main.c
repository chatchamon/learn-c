#include <stdio.h>

int main()
{
    int n = 10;
    int * pointer_to_n = &n;
    
    printf("n=%d ,pointer_to_=%d, deref=%d\n", n, pointer_to_n, *pointer_to_n);
    
    n += 1;
    printf("n=%d ,pointer_to_=%d, deref=%d\n", n, pointer_to_n, *pointer_to_n);
    
    //test
    if(pointer_to_n != &n) return 1;
    if(*pointer_to_n != 11) return 1;
    
    printf("Done!\n");
    return 0;
}
