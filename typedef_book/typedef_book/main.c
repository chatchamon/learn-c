#include <stdio.h>
#include <string.h>

typedef struct Books{
    char title[50];
    char author[50];
    int book_id;
}Books;

int main()
{
    Books book;
    strcpy(book.title, "C programming");
    strcpy(book.author, "Nuha Ali");
    book.book_id = 6495407;
    
    printf("Book title:     %s\n", book.title);
    printf("Author:         %s\n", book.author);
    printf("Book id:        %d\n", book.book_id);
    
    return 0;
}
